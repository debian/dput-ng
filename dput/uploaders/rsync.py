# -*- coding: utf-8 -*-
# vim: tabstop=4 expandtab shiftwidth=4 softtabstop=4

# Copyright (c) 2012, 2023 dput authors
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your Option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.    See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
# 02110-1301, USA.
"""
Rsync Uploader implementation
"""

import os.path

from dput.core import logger
from dput.uploader import AbstractUploader
from dput.exceptions import UploadException
from dput.uploaders._ssh_common import get_ssh_config, find_username
from dput.util import run_command


class RsyncUploadException(UploadException):
    """
    Thrown in the event of a problem connecting, uploading to or
    terminating the connection with the remote server. This is
    a subclass of :class:`dput.exceptions.UploadException`.
    """
    pass


class RsyncUploader(AbstractUploader):
    """
    Provides an interface to upload files through rsync.

    This is a subclass of :class:`dput.uploader.AbstractUploader`
    """

    def initialize(self, **kwargs):
        """
        See :meth:`dput.uploader.AbstractUploader.initialize`
        """
        ssh_base = ["ssh", "-x"]
        if "port" in self._config:
            ssh_base += ["-p", self._config["port"]]
        self._rsync_base = ["rsync", "--copy-links", "--partial", "-za",
                            "--chmod=644", "-e", " ".join(ssh_base)]
        fqdn = self._config["fqdn"]
        self._rsync_host = "%s@%s" % (find_username(self._config,
                                                    get_ssh_config(fqdn)),
                                      fqdn)
        logger.debug("Using rsync to upload to %s" % (self._rsync_host))

    def upload_file(self, filename, upload_filename=None):
        """
        See :meth:`dput.uploader.AbstractUploader.upload_file`
        """

        if not upload_filename:
            upload_filename = os.path.basename(filename)

        incoming = self._config["incoming"]
        targetfile = "%s:%s" % (self._rsync_host,
                                os.path.join(incoming, upload_filename))
        rsync = self._rsync_base + [filename, targetfile]
        (_, e, x) = run_command(rsync)
        if x != 0:
            raise RsyncUploadException("Failed to upload %s to %s: %s" % (
                upload_filename, targetfile, e)
            )

    def shutdown(self):
        """
        See :meth:`dput.uploader.AbstractUploader.shutdown`
        """
        pass
